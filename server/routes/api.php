<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/



Route::post('store', 'TaskController@store')->name('store');
Route::get('tasks', 'TaskController@index')->name('tasks');
Route::get('task/{id}', 'TaskController@show');
Route::post('update', 'TaskController@update');
Route::delete('delete-task/{id}', 'TaskController@destroy');

Route::middleware('cross')->group(function () {

});


Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
