import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {HomeComponent} from './components/home/home.component';
import {TaskManagerComponent} from './components/task-manager/task-manager.component';
import {TodoComponent} from './components/todo/todo.component';
import {TaskEditComponent} from './components/task-manager/task-edit/task-edit.component';
import {TaskListComponent} from './components/task-manager/task-list/task-list.component';


const routes: Routes = [
  {path:'', redirectTo:'home', pathMatch:'full'},
  {path:'home', component:HomeComponent},
  {path:'task', component:TaskManagerComponent},
  {path:'tasks', component:TaskListComponent},
  {path:'task/:id/edit', component:TaskEditComponent},
  {path:'todo', component:TodoComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
