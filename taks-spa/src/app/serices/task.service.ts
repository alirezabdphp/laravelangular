import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import {environment} from '../../environments/environment';


@Injectable({
  providedIn: 'root'
})

export class TaskService {

  // Base url
  baseurl = environment.apiUrl

  constructor(private http: HttpClient) { }

  // Http Headers
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }

  // Add Task
  store(data): Observable<any> {
    return this.http.post<any>(this.baseurl + '/store', JSON.stringify(data), this.httpOptions);
  }

  // Get Tasks
  tasks(): Observable<any> {
      return this.http.get<any>(this.baseurl + '/tasks');
  }

  // Delete Task
  delete(task_id): Observable<any> {
    return this.http.delete<any>(this.baseurl + '/delete-task/'+task_id, this.httpOptions);
  }

  // Get Task
  task(task_id):Observable<any>{
      return this.http.get(this.baseurl + '/task/' +task_id);
  }

  // Update Task

  update(data){
      return this.http.post<any>(this.baseurl + '/update', JSON.stringify(data), this.httpOptions);
  }
}
