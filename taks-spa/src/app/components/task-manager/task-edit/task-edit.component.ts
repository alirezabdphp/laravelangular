import { Component, OnInit } from '@angular/core';
import { ActivatedRoute , Router } from '@angular/router';
import { TaskService} from '../../../serices/task.service';

@Component({
  selector: 'app-task-edit',
  templateUrl: './task-edit.component.html',
  styleUrls: ['./task-edit.component.css']
})
export class TaskEditComponent implements OnInit {
  task: object = [];
  constructor(
      private activeRoute:ActivatedRoute,
      private taskService:TaskService,
      private router:Router
  ) {}

  ngOnInit(): void {
    this.activeRoute.params.subscribe((data)=>{
      this.taskService.task(data.id).subscribe((data)=>{
          this.task = data;
      });
    });
  }

  updateTask(e){
      e.preventDefault();
      const target = e.target;
      this.taskService.update(this.task).subscribe((data)=>{
          this.router.navigateByUrl('tasks');
      });
  }
}
