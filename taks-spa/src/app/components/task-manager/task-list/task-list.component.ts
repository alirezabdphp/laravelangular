import { Component, OnInit } from '@angular/core';
import {TaskService} from '../../../serices/task.service';
import {MessageService} from '../../../serices/message.service';

@Component({
  selector: 'app-task-list',
  templateUrl: './task-list.component.html',
  styleUrls: ['./task-list.component.css']
})
export class TaskListComponent implements OnInit {

  tasks: any = [];
  constructor(private taskService:TaskService, private msgService: MessageService) { }

  ngOnInit(): void {
    this.getTasks();
    this.msgService.getMessage().subscribe(data => {
      this.getTasks();
    });
  }

  // Issues list
  getTasks() {
    return this.taskService.tasks().subscribe((data: {}) => {
      this.tasks = data;
    });
  }

  // Delete Task
  deleteTask(task_id){
    this.taskService.delete(task_id).subscribe((data: {}) => {
      this.msgService.setMessage('Task Deleted');
    });
  }

}
