import { Component, OnInit } from '@angular/core';
import {TaskService} from '../../../serices/task.service';
import {subscribeTo} from 'rxjs/internal-compatibility';
import {MessageService} from '../../../serices/message.service';



@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit {

  task = {
    title: ''
  };

  constructor(private taskService:TaskService, private msgService: MessageService) { }

  ngOnInit(): void {
  }

  storeTask(e){
    e.preventDefault();
    const target = e.target;
    this.taskService.store(this.task).subscribe(data => {
      this.task.title = '';
      this.msgService.setMessage('Update Tasks');
    });
  }
}
